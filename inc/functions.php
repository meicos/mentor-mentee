<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

function uploadImage(){

	$target_dir = "images/uploads/";
	$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
	$uploadOk = 1;
	$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
	// Check if image file is a actual image or fake image
	if(isset($_POST["submit"])) {
	    $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
	    if($check !== false) {
	        echo "File is an image - " . $check["mime"] . ".";
	        $uploadOk = 1;
	    } else {
	        echo "File is not an image.";
	        $uploadOk = 0;
	    }
	}
	// Check if file already existss
	if (file_exists($target_file)) {
	    echo "Sorry, file already exists.";
	    $uploadOk = 0;
	}
	// Check file size
	if ($_FILES["fileToUpload"]["size"] > 1000000) {
	    echo "Sorry, your file is too large.";
	    $uploadOk = 0;
	}
	// Allow certain file formats
	if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
	&& $imageFileType != "gif" ) {
	    echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
	    $uploadOk = 0;
	}
	// Check if $uploadOk is set to 0 by an error
	if ($uploadOk == 0) {
	    echo "Sorry, your file was not uploaded.";
	// if everything is ok, try to upload file
	} else {
	    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
	        echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
	    } else {
	        echo "Sorry, there was an error uploading your file.";
	    }
	}
	return $target_file;
}

class connect_db {

	public $category, $main_title, $main_lead, $title, $content, $link;

	public function __construct($category, $main_title, $main_lead, $title, $content, $link) {
		$this->category = $category;
		$this->main_title = $main_title;
		$this->main_lead = $main_lead;
		$this->title = $title;
		$this->content = $content;
		$this->link = $link;
	}

	public function save_db($img_link) {
		$this->img_link = $img_link;
		require('.connect');

		// Create connection
		$conn = new mysqli($servername, $username, $password, $dbname);
		$sql = "INSERT INTO lessons (category, main_title, main_lead, title, content, img_link, link)
		VALUES ('$this->category', '$this->main_title', '$this->main_lead', '$this->title', '$this->content','$this->img_link', '$this->link')";

		if ($conn->query($sql) === TRUE) {
		    echo "New record created successfully";
		} else {
		    echo "Error: " . $sql . "<br>" . $conn->error;
		}

		$conn->close();
	}


}

?>