<?php require('header.php'); ?>

<!--jumbotron -->
<div class="jumbotron">
	<div class="container text-left">
		<div class="row">
			<div class="col-sm-7">
				<h1>Built for <br>Challengers</h1>
				<p><strong><span class="glyphicon glyphicon-link"></span> Menter-Mentee</strong> is a place for the people who inspired by the way you work. From <strong>beginner</strong> to <strong>advance</strong>, you can share tha way how to learn programming alongside millions of other developers.</p>
			</div>
			<div class="col-sm-4 col-sm-offset-1">
				<form>
					<div class="form-group">
						<input type="username" class="form-control" id="username" placeholder="Pick a username">
					</div>
					<div class="form-group">
						<input type="email" class="form-control" id="email" placeholder="Your email address">
					</div>
					<div class="form-group">
						<input type="password" class="form-control" id="pwd" placeholder="create a password">
						<span class="small">Use at least one letter, one numeral, and seven characters.</span>
					</div>
					<div class="form-group">
						<button type="submit" class="btn btn-block btn-primary btn-lg">Sign up</button>
					</div>
					<span class="small">Sign up with:</span>
					<div class="form-group">
						<div class="btn-group btn-group-justified">
							<a href="inc/fblogin.php" class="btn btn-primary"><i class="devicon-facebook-plain"></i></a>
							<a href="#" class="btn btn-info"><i class="devicon-twitter-plain"></i></a>
							<a href="#" class="btn btn-danger"><i class="devicon-google-plain"></i></a>
							<a href="#" class="btn btn-default"><i class="devicon-github-plain"></i></a>
						</div>
						<!--<span class="small">By clicking "Sign up", you agree to our <a href="">terms of use</a> and <a href="">privacy policy</a>.</span>-->
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<!-- / .jumbotron --> 

<!-- contents -->
<section id="service">
	<div class="container">
	
		<div class="row text-center">
			<h2>NEW POSTS</h2>
		</div>
		<br>

		<div class="row row-1">
			<div class="col-md-4 ser-col-4">
				<div class="ser-col ser-1">
					<div class="icon-col">
						<i class="devicon-wordpress-plain-wordmark colored"></i>
					</div>
					<h3>this is a loerem spe</h3>
					<p>Lorem ipsum dolor sit amet, consectetur
						adipiscing elit. Curabitur
						sit amet enim mauris. Fusce
						hendrerit velit vitae enim
						hendrerit ultrices. </p>
					<a href="page.php" class="btn btn-link"><span class="glyphicon glyphicon-play-circle"></span> Read More</a>
					<span class="circle hidden-xs">
					<i class="fa fa-circle-thin" aria-hidden="true"></i>
					</span>
				</div>
			</div>

			<div class="col-md-4 ser-col-4">
				<div class="ser-col ser-2">
					<div class="icon-col">
						<i class="devicon-bootstrap-plain-wordmark colored"></i>
					</div>
					<h3>this is a loerem spe</h3>
					<p>Lorem ipsum dolor sit amet, consectetur
						adipiscing elit. Curabitur
						sit amet enim mauris. Fusce
						hendrerit velit vitae enim
						hendrerit ultrices. </p>
					<a href="page.php" class="btn btn-link"><span class="glyphicon glyphicon-play-circle"></span> Read More</a>
					<span class="circle hidden-xs">
					<i class="fa fa-circle-thin" aria-hidden="true"></i>
					</span>
				</div>
			</div>

			<div class="col-md-4 ser-col-4-l">
				<div class="ser-col ser-3">
					<div class="icon-col">
						<i class="devicon-bitbucket-plain-wordmark colored"></i>
					</div>
					<h3>this is a loerem spe</h3>
					<p>Lorem ipsum dolor sit amet, consectetur
						adipiscing elit. Curabitur
						sit amet enim mauris. Fusce
						hendrerit velit vitae enim
						hendrerit ultrices. </p>
					<a href="page.php" class="btn btn-link"><span class="glyphicon glyphicon-play-circle"></span> Read More</a>
				</div>
			</div>
		</div>
		<!--=====row 1============-->

	</div>
</section>

<?php require('footer.php'); ?>