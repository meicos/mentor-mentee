<?php require('header.php');?>
<?php require('inc/functions.php');?>
<?php 
error_reporting(E_ALL);
ini_set('display_errors', 1);

if ($_POST['submit']) {
$imageLink = uploadImage(); // getting the return value of target_image

$category = $_POST['category'];
$main_title = $_POST['main_title'];
$main_lead = $_POST['main_lead'];
$title = $_POST['title'];
$content = $_POST['content'];
$link = $_POST['link'];

$save_lesson = new connect_db($category, $main_title, $main_lead, $title, $content, $link);
$save_lesson->save_db($imageLink);
header('Location: page.php');

}
?>

<?php

$formTags = "<div id='formSet'>
						<div class='form-group'>
							<label for='input-mail' class='col-sm-2 control-label'>title</label>
							<div class='col-sm-10'>
								<input type='text' class='form-control' id='input-ttl-replacecount' placeholder='title' required>
							</div>
						</div>
						<div id='formSetSub'>
							<div class='col-sm-11'>
								<div class='form-group'>
									<label class='col-sm-2 control-label'>contents</label>
									<div class='col-sm-10'>
										<textarea class='form-control' rows='5' id='input-cont-replacecount' required></textarea>
									</div>
								</div>
								<div class='form-group'>
									<div class='col-sm-offset-2 col-sm-10'>
										<div class='input-group'>
												<label class='input-group-btn'>
														<span class='btn btn-primary'>
																Choose File<input type='file' style='display:none' class='uploadFile'>
														</span>
												</label>
												<input type='text' id='input-img-replacecount' class='form-control' readonly>
										</div>
									</div>
								</div>
							</div>
						</div><!-- / #formSetSub -->
						<div class='col-sm-1'>
							<div class='form-group'>
								<a class='btn btn-default' id='addFormSub'>+ add</a>
							</div>
						</div>
					</div><!-- / #formSet -->";
$formTags = str_replace("\n" , "\\n" , $formTags);

$formSubTags = "<div id='formSetSub'>
							<div class='col-sm-11'>
								<div class='form-group'>
									<label class='col-sm-2 control-label'>contents</label>
									<div class='col-sm-10'>
										<textarea class='form-control' rows='5' id='input-cont-replacecount' required></textarea>
									</div>
								</div>
								<div class='form-group'>
									<div class='col-sm-offset-2 col-sm-10'>
										<div class='input-group'>
												<label class='input-group-btn'>
														<span class='btn btn-primary'>
																Choose File<input type='file' style='display:none' class='uploadFile'>
														</span>
												</label>
												<input type='text' id='input-img-replacecount' class='form-control' readonly>
										</div>
									</div>
								</div>
							</div>
						</div><!-- / #formSetSub -->";
$formSubTags = str_replace("\n" , "\\n" , $formSubTags);

$linkTags = "<div class='form-group'>
							<label for='input-mail' class='col-sm-2 control-label'>link</label>
							<div class='col-sm-10'>
								<input type='text' class='form-control' id='input-link-replacecount'>
							</div>
						</div>";
$linkTags = str_replace("\n" , "\\n" , $linkTags);

?>

<div id="page">
	<div class="container text-center">
		<h1>Create New Lesson</h1>
		<div class="row">
			<div class="col-sm-11 text-left">
				<form action="" method="post" enctype="multipart/form-data" class="form-horizontal">
					<hr>
					<div class="form-group">
						<label class="col-sm-2 control-label">category</label>
						<div class="col-sm-10">
								<label class="radio-inline"><input type="radio" name="category" checked><i class="devicon-wordpress-plain-wordmark colored"></i> wordpress</label>
								<label class="radio-inline"><input type="radio" name="category"><i class="devicon-bootstrap-plain-wordmark colored"></i> bootstrap</label>
								<label class="radio-inline"><input type="radio" name="category"><i class="devicon-bitbucket-plain-wordmark colored"></i> bitbucket</label>
								<label class="radio-inline"><input type="radio" name="category"><i class="devicon-css3-plain-wordmark colored"></i> css3</label>
								<label class="radio-inline"><input type="radio" name="category"><i class="devicon-git-plain-wordmark colored"></i> git</label>
								<label class="radio-inline"><input type="radio" name="category"><i class="devicon-html5-plain-wordmark colored"></i> html5</label><br>
								<label class="radio-inline"><input type="radio" name="category"><i class="devicon-javascript-plain colored"></i> javascript</label>
								<label class="radio-inline"><input type="radio" name="category"><i class="devicon-jquery-plain-wordmark colored"></i> jquery</label>
								<label class="radio-inline"><input type="radio" name="category"><i class="devicon-linux-plain colored"></i> linux</label>
								<label class="radio-inline"><input type="radio" name="category"><i class="devicon-mysql-plain-wordmark colored"></i> mysql</label>
								<label class="radio-inline"><input type="radio" name="category"><i class="devicon-linux-plain colored"></i> php</label>
								<label class="radio-inline"><input type="radio" name="category"><i class="devicon-ubuntu-plain-wordmark colored"></i> ubuntu</label>
						</div>
					</div>
					<hr>
					<div class="form-group">
						<label for="input-name" class="col-sm-2 control-label">Main title</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" name="main_title" id="input-main-ttl" placeholder="Main title" required>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">Main lead</label>
						<div class="col-sm-10">
							<textarea class="form-control" rows="3" name="main_lead" id="input-cont-1" required></textarea>
						</div>
					</div>
					<hr>
					<div id="formSet">
						<div class="form-group">
							<label for="input-mail" class="col-sm-2 control-label">title</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" name="title" id="input-ttl-1" placeholder="title" required>
							</div>
						</div>
						<div id="formSetSub">
							<div class="col-sm-11">
								<div class="form-group">
									<label class="col-sm-2 control-label">contents</label>
									<div class="col-sm-10">
										<textarea class="form-control" rows="5" name="content" id="input-cont-1" required></textarea>
									</div>
								</div>
								<div class="form-group">
									<div class="col-sm-offset-2 col-sm-10">
										<div class="input-group">
												<label class="input-group-btn">
														<span class="btn btn-primary">
																Choose File<input type="file" style="display:none" class="uploadFile" name="fileToUpload">
														</span>
												</label>
												<input type="text" id="input-img-1" class="form-control" readonly>
										</div>
									</div>
								</div>
							</div>
						</div><!-- / #formSetSub -->
						<div class="col-sm-1">
							<div class="form-group">
								<a class="btn btn-default" id="addFormSub">+ add</a>
							</div>
						</div>
					</div><!-- / #formSet -->
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-10">
							<a class="btn btn-default" id="addForm">+ add form</a>
						</div>
					</div>
					<hr>
					<div id="linkSet">
						<div class="form-group">
							<label for="input-mail" class="col-sm-2 control-label">link</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" name="link" id="input-link-1">
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-10">
							<a class="btn btn-default" id="addLink">+ add link</a>
						</div>
					</div>
					<hr>
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-10">
							<input type="submit" name="submit" value="Submit" class="btn btn-primary">
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	<!-- /container --> 
</div>
<!-- /page -->

<script>
//imageform
$(document).on('change', ':file', function() {
    var input = $(this),
    numFiles = input.get(0).files ? input.get(0).files.length : 1,
    label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
    input.parent().parent().next(':text').val(label);
});

//addForm
var countForm = 1;
$(function(){
  $('#addForm').click(function(){
		countForm++;
		var formSet = "<?php echo str_replace( 'replacecount' , '" + countForm + "' , $formTags); ?>";
		$('#formSet').append(formSet);
	});
});

//addFormSub
var countFormSub = 1;
$(function(){
  $('#addFormSub').click(function(){
		countFormSub++;
		var formSetSub = "<?php echo str_replace( 'replacecount' , '" + countForm + "-" + countFormSub + "' , $formSubTags); ?>";
		$('#formSetSub').append(formSetSub);
	});
});

//addLink
var countLink = 1;
$(function(){
  $('#addLink').click(function(){
		countLink++;
		var linkSet = "<?php echo str_replace( 'replacecount' , '" + countLink + "' , $linkTags); ?>";
		$('#linkSet').append(linkSet);
	});
});
</script>

<?php require('footer.php'); ?>