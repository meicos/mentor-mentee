<!DOCTYPE html>
<html lang="en">
<head>
<title>Menter-Mentee - Menter-Mentee is a place for the people who inspired by the way you work. From beginner to advance, you can share tha way how to learn programming alongside millions of other developers.</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="shortcut icon" href="/favicon.ico">
<link rel="stylesheet" href="themes/default/css/bootstrap.min.css">
<link rel="stylesheet" href="themes/default/css/my_index.css">

<!--devicon -->
<link rel="stylesheet" href="https://cdn.rawgit.com/konpa/devicon/4f6a4b08efdad6bb29f9cc801f5c07e263b39907/devicon.min.css">

<script src="themes/default/js/jquery.min.js"></script>
<script src="themes/default/js/bootstrap.min.js"></script>

<script>
<!--scrollspy -->
$('#page .container').scrollspy({target: "#side .list-group"});
</script>

</head>
<body id="<?php echo"my_" . basename($_SERVER['PHP_SELF'], '.php'); ?>">

<!--Nav -->
<nav class="navbar navbar-inverse">
	<div class="container-fluid">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
			<a class="navbar-brand" href="index.php"><span class="glyphicon glyphicon-link"></span> Menter-Mentee</a> </div>
		<div class="collapse navbar-collapse" id="myNavbar">
			<ul class="nav navbar-nav">
				<li class="active"><a href="index.php">Home</a></li>
				<li class="dropdown"> <a class="dropdown-toggle" data-toggle="dropdown" href="#">Learn <span class="caret"></span></a>
					<ul class="dropdown-menu">
						<li><a href="list.php?paths"><span class="glyphicon glyphicon-th-large"></span> Paths</a></li>
						<li><a href="list.php?courses"><span class="glyphicon glyphicon-th-list"></span> Courses</a></li>
					</ul>
				</li>
				<li><a href="#">Support</a></li>
				<li><a href="#">Contact</a></li>
			</ul>
			<ul class="nav navbar-nav navbar-right">
				<li><a href="form.php"><span class="glyphicon glyphicon-pencil"></span> write</a></li>
				<li><a href="" data-toggle="modal" data-target="#myLogin"><span class="glyphicon glyphicon-log-in"></span> Sign in</a></li>
			</ul>
		</div>
	</div>
</nav><!-- / .navbar -->

