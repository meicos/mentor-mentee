	<section id="side" class="col-sm-3">
		<button type="button" class="btn btn-lg btn-warning btn-block" style="margin-top:20px;">Join this Lesson</button>
		<hr>
		<div class="panel-group">
			<div class="panel panel-default">
				<div class="panel-heading text-center"><strong>Syllabus</strong></div>
				<div class="panel-body">
					<p><a href="#section0">Overview</a></p>
					<p><a href="#section1">1.Know the outline</a></p>
					<p><a href="#section2">2.Learn basic <small>※Try free plans.</small></a></p>
					<p><a href="#section3">3.Try to create</a></p>
					<p><a href="#section4">what else</a></p>
				</div>
			</div><!-- / .panel panel-default -->
			<div class="panel panel-default">
				<div class="panel-heading text-center"><strong>Your status</strong></div>
				<div class="panel-body">
					<p><small>You're complete this lesson</small></p>
					<div class="progress">
						<div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:40%">40%</div>
					</div>
				</div>
			</div><!-- / .panel panel-default -->
			<div class="panel panel-default">
				<div class="panel-heading text-center"><strong>Your memo</strong></div>
				<div class="panel-body clearfix">
					<form>
						<div class="form-group">
							<textarea class="form-control" rows="5" id="comment"></textarea>
						</div>
						<div class="btn-group btn-group-xs pull-right">
							<button type="button" class="btn btn-primary">save</button>
							<button type="button" class="btn">reset</button>
						</div>
					</form>
				</div>
			</div><!-- / .panel panel-default -->
			<div class="panel panel-default">
				<div class="panel-heading text-center"><strong>Your questions</strong></div>
				<div class="panel-body">
					<div class="media">
						<div class="media-left">
							<img src="themes/default/img/thumbnails.png" class="media-object" width="20px">
						</div>
						<div class="media-body">
							<h5 class="media-heading">John Doe <small><i>Posted on February 19, 2016</i></small></h5>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
						</div>
					</div><!-- / .media -->
					<div class="media">
						<div class="media-left">
							<img src="themes/default/img/thumbnails_2.png" class="media-object" width="20px">
						</div>
						<div class="media-body">
							<h5 class="media-heading">John Doe <small><i>Posted on February 19, 2016</i></small></h5>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
						</div>
					</div><!-- / .media -->
					<button type="button" class="btn btn-primary btn-xs pull-right">more</button>
				</div>
			</div><!-- / .panel panel-default -->
		</div><!-- / .panel-group -->
		
		<div class="panel-group">
			<div class="panel panel-default">
				<div class="panel-heading text-center"><strong>You're studying:</strong></div>
				<div class="panel-body">
					<div class="item">
						<p><a href=""><i class="devicon-html5-plain colored"></i> HTML for beginners...</a></p>
						<div class="progress">
							<div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width:60%">60%</div>
						</div>
					</div><!-- / .item -->
					<div class="item">
						<p><a href=""><i class="devicon-css3-plain-wordmark colored"></i> CSS for beginners...</a></p>
						<div class="progress">
							<div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:40%">40%</div>
						</div>
					</div><!-- / .item -->
					<div class="item">
						<p><a href=""><i class="devicon-bitbucket-plain-wordmark colored"></i> Bitbucket training team</a></p>
						<div class="progress">
							<div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width:80%">80%</div>
						</div>
					</div><!-- / .item -->
					<div class="item">
						<p><a href=""><i class="devicon-linux-plain colored"></i> Linux advanced lessonn...</a></p>
						<div class="progress">
							<div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100" style="width:10%">10%</div>
						</div>
					</div><!-- / .item -->
					<button type="button" class="btn btn-primary btn-xs pull-right">more</button>
				</div>
			</div><!-- / .panel panel-default -->
		</div><!-- / .panel-group -->
	</section><!-- / #side .col-sm-4 -->