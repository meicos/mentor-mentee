<?php require('header.php'); ?>
<?php require('inc/functions.php');?>

<?php 
error_reporting(E_ALL);
ini_set('display_errors', 1);



$category = $_POST['category'];
$main_title = $_POST['main_title'];
$main_lead = $_POST['main_lead'];
$title = $_POST['title'];
$content = $_POST['content'];
$link = $_POST['link'];

$save_lesson = new connect_db($category, $main_title, $main_lead, $title, $content, $link);
$save_lesson->save_db($imageLink);
header('Location: page.php');
?>

<!-- breadcrumb -->
<ol class="breadcrumb">
	<li><a href="index.php">Home</a></li>
	<li><a href="list.php?paths">list</a></li>
	<li><a href="#">HTML5</a></li>
	<li class="active">Continue your learning by starting HTML & CSS.</li>
</ol><!-- / .breadcrumb -->

<!-- contents -->
<div class="container" data-spy="scroll" data-target="#myScrollspy" data-offset="15">
	<section id="main" class="col-sm-9">
		<div class="row">
			<div class="col-sm-2">
				<div class="ser-col ser-6">
					<div class="icon-col"> <i class="devicon-html5-plain colored"></i> </div>
				</div>
			</div>
			<div class="col-sm-10">
				<h2>Continue your learning by starting HTML & CSS.</h2>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
			</div>
		</div>
		<!-- / .row -->

		<!-- well -->
		<div class="well">
		<div class="row" style="padding-top:20px;">
		<div class="col-sm-9">
			<div class="media">
				<div class="media-left">
					<a href=""><img src="themes/default/img/thumbnails_2.png" class="media-object" style="width:60px"></a>
				</div>
				<div class="media-body">
					<h4 class="media-heading"><small><i>This lesson is created by :</i></small> <a href="">Meico</a></h4>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
					<ul class="list-inline">
						<li class="bg-info">this mentor's lesson:<strong>87</strong></li>
						<li class="bg-info">this mentor's mentee:<strong>1427</strong></li>
					</ul>
				</div>
			</div>
			</div>
			<div class="col-sm-3 text-center">
			<p>taking this course:<br><h2><strong>108 </strong><small>people</small></h2></p>
			<p><strong>967 </strong><small>complete.</small></p>
				
			</div>
			</div>
		</div><!-- / .well -->

		
		<section id="section0">
			<div class="row">
				<div class="col-sm-12">
					<h3>Overview</h3>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<h4>COURSE OUTCOMES</h4>
					<p>By the end of this course, you'll learn the basics of HTML and CSS and how to structure and style your webpage.</p>
					<p>Why learn HTML and CSS? Everything you see in a website is a result of the combination of HTML and CSS. With these two languages, you will have the skills you need to bring your website design to life.</p>
				</div>
			</div>
		</section>
		<!-- / #section0 -->
		
		<hr>
		
		<section id="section1">
			<div class="row">
				<div class="col-sm-12">
					<h3>1.Know the outline</h3>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<h4>dotinstall</h4>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-7">
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
					<h5>HTML</h5>
					<ul>
						<li><a href="http://dotinstall.com/lessons/basic_html_v3" target="_blank">http://dotinstall.com/lessons/basic_html_v3</a></li>
					</ul>
					<h5>CSS</h5>
					<ul>
						<li><a href="http://dotinstall.com/lessons/basic_css_v3" target="_blank">http://dotinstall.com/lessons/basic_css_v3</a></li>
						<li><a href="http://dotinstall.com/lessons/basic_css3_v2" target="_blank">http://dotinstall.com/lessons/basic_css3_v2</a></li>
						<li><a href="http://dotinstall.com/lessons/basic_css_layout" target="_blank">http://dotinstall.com/lessons/basic_css_layout</a></li>
					</ul>

				</div>
				<div class="col-sm-5"> <img src="images/image_00001.png" class="img-thumbnail" alt="dotinstall"> </div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<h4>skillhub</h4>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-7">
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
					<h5>making website</h5>
					<ul>
						<li><a href="http://skillhub.jp/courses/61" target="_blank">http://skillhub.jp/courses/61</a></li>
						<li><a href="http://skillhub.jp/courses/49" target="_blank">http://skillhub.jp/courses/49</a></li>
					</ul>
				</div>
				<div class="col-sm-5"> <img src="images/image_00002.png" class="img-thumbnail" alt="skillhub"> </div>
			</div>
		</section>
		<!-- / #section1 -->
		
		<hr>
		
		<section id="section2">
			<div class="row">
				<div class="col-sm-12">
					<h3>2.Learn basic <small>※Try free plans.</small></h3>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<h4>Progate</h4>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-7">
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
					<ul>
						<li><a href="https://prog-8.com/languages/html" target="_blank">https://prog-8.com/languages/html</a></li>
					</ul>
				</div>
				<div class="col-sm-5"> <img src="images/image_00003.png" class="img-thumbnail" alt="Progate"> </div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<h4>codeprep</h4>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-7">
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
					<ul>
						<li><a href="https://codeprep.jp/books/1" target="_blank">https://codeprep.jp/books/1</a></li>
						<li><a href="https://codeprep.jp/books/2" target="_blank">https://codeprep.jp/books/2</a></li>
						<li><a href="https://codeprep.jp/books/17" target="_blank">https://codeprep.jp/books/17</a></li>
						<li><a href="https://codeprep.jp/books/12" target="_blank">https://codeprep.jp/books/12</a></li>
					</ul>
				</div>
				<div class="col-sm-5"> <img src="images/image_00004.png" class="img-thumbnail" alt="codeprep"> </div>
			</div>
		</section>
		<!-- / #section2 -->
		
		<hr>
		
		<section id="section3">
			<div class="row">
				<div class="col-sm-12">
					<h3>3.Try to create</h3>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<h4>dotinstall</h4>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-7">
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
					<ul>
						<li><a href="http://dotinstall.com/lessons/website_html_v2" target="_blank">http://dotinstall.com/lessons/website_html_v2</a></li>
					</ul>
				</div>
				<div class="col-sm-5"> <img src="images/image_00001.png" class="img-thumbnail" alt="dotinstall"> </div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<h4>codeprep</h4>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-7">
					<ul>
						<li><a href="https://codeprep.jp/books/49" target="_blank">https://codeprep.jp/books/49</a></li>
						<li><a href="https://codeprep.jp/books/13" target="_blank">https://codeprep.jp/books/13</a></li>
						<li><a href="https://codeprep.jp/books/4" target="_blank">https://codeprep.jp/books/4</a></li>
						<li><a href="https://codeprep.jp/books/10" target="_blank">https://codeprep.jp/books/10</a></li>
						<li><a href="https://codeprep.jp/books/8" target="_blank">https://codeprep.jp/books/8</a></li>
					</ul>
				</div>
				<div class="col-sm-5"> <img src="images/image_00004.png" class="img-thumbnail" alt="codeprep"> </div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<h4>skillhub</h4>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-7">
					<ul>
						<li><a href="http://skillhub.jp/courses/38" target="_blank">http://skillhub.jp/courses/38</a></li>
						<li><a href="http://skillhub.jp/courses/57" target="_blank">http://skillhub.jp/courses/57</a></li>
					</ul>
				</div>
				<div class="col-sm-5"> <img src="images/image_00002.png" class="img-thumbnail" alt="skillhub"> </div>
			</div>
		</section>
		<!-- / #section3 -->
		
		<hr>
		
		<section id="section4">
			<div class="row">
				<div class="col-sm-12">
					<h3>what else</h3>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<h4>dotinstall</h4>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-7">
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
					<ul>
						<li><a href="https://codeprep.jp/books?tag=HTML&tag=CSS" target="_blank">https://codeprep.jp/books?tag=HTML&tag=CSS</a></li>
					</ul>
				</div>
				<div class="col-sm-5"> <img src="images/image_00001.png" class="img-thumbnail" alt="dotinstall"> </div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<h4>recommended website</h4>
					<div class="list-group">
						<a href="http://coliss.com/" class="list-group-item" target="_blank">coliss <span class="glyphicon glyphicon-play-circle pull-right"></span></a>
						<a href="http://www.webcyou.com/" class="list-group-item" target="_blank">ウェブ帳 <span class="glyphicon glyphicon-play-circle pull-right"></span></a>
						<a href="http://www.webcreatorbox.com/" class="list-group-item" target="_blank">Webクリエイターボックス <span class="glyphicon glyphicon-play-circle pull-right"></span></a>
						<a href="https://www.w3schools.com/" class="list-group-item" target="_blank">w3schools.com <span class="glyphicon glyphicon-play-circle pull-right"></span></a>
					</div>
				</div>
			</div>
		</section>
		<!-- / #section4 -->

		<div style="margin-bottom:80px;">
			<h3><small>Related words</small></h3>
			<p>
			<a href=""><span class="label label-default">HTML</span></a>
			<a href=""><span class="label label-primary">CSS</span></a>
			<a href=""><span class="label label-success">Javascript</span></a>
			<a href=""><span class="label label-info">jQuery</span></a>
			</p>
		</div>

		<div>
			<ul class="nav nav-tabs">
				<li class="active"><a data-toggle="tab" href="#home">FAQ</a></li>
				<li><a data-toggle="tab" href="#menu1">comment</a></li>
			</ul>
			<div class="tab-content">
				<div id="home" class="tab-pane fade in active">
					<h3>FAQ</h3>
					<div class="media">
						<div class="media-left"> <img src="themes/default/img/thumbnails.png" class="media-object" width="45px"> </div>
						<div class="media-body">
							<h5 class="media-heading">John Doe <small><i>Posted on February 19, 2016</i></small></h5>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
						</div>
					</div>
					<!-- / .media -->
					<hr>
					<div class="media">
						<div class="media-body">
							<h5 class="media-heading">John Doe <small><i>Posted on February 19, 2016</i></small></h5>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
						</div>
						<div class="media-right"> <img src="themes/default/img/thumbnails_2.png" class="media-object" width="30px"> </div>
					</div>
					<!-- / .media -->
					<div class="media">
						<div class="media-body">
							<h5 class="media-heading">John Doe <small><i>Posted on February 19, 2016</i></small></h5>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
						</div>
						<div class="media-right"> <img src="themes/default/img/thumbnails_2.png" class="media-object" width="30px"> </div>
						<a type="button" class="btn btn-link pull-right"><span class="glyphicon glyphicon-play-circle"></span> Read More</a> </div>
					<!-- / .media -->
					<hr>
					<div class="form-group">
						<label for="comment">send question:</label>
						<textarea class="form-control" rows="5" id="comment"></textarea>
					</div>
					<button type="button" class="btn btn-primary pull-right">submit</button>
				</div>
				<div id="menu1" class="tab-pane fade">
					<h3>Comment from mentees</h3>
					<div class="media">
						<div class="media-left"> <img src="themes/default/img/thumbnails.png" class="media-object" width="30px"> </div>
						<div class="media-body">
							<h5 class="media-heading">John Doe <small><i>Posted on February 19, 2016</i></small></h5>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
						</div>
					</div>
					<!-- / .media -->
					<div class="media">
						<div class="media-left"> <img src="themes/default/img/thumbnails_2.png" class="media-object" width="30px"> </div>
						<div class="media-body">
							<h5 class="media-heading">John Doe <small><i>Posted on February 19, 2016</i></small></h5>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
						</div>
					</div>
					<!-- / .media -->
					<div class="media">
						<div class="media-left"> <img src="themes/default/img/thumbnails.png" class="media-object" width="30px"> </div>
						<div class="media-body">
							<h5 class="media-heading">John Doe <small><i>Posted on February 19, 2016</i></small></h5>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
						</div>
						<a class="btn btn-link pull-right"><span class="glyphicon glyphicon-play-circle"></span> Read More</a> </div>
					<!-- / .media -->
					<hr>
					<div class="form-group">
						<label for="comment">send comment:</label>
						<textarea class="form-control" rows="5" id="comment"></textarea>
					</div>
					<button type="button" class="btn btn-primary pull-right">submit</button>
				</div>
			</div>
		</div>

	</section>
	<!-- / #main .col-sm-8 -->
	
	<?php require('side.php'); ?>
</div>
<!-- / #  container-->
<?php require('footer.php'); ?>