<?php //require('fblogin/fblogin.php'); ?>

<!-- Modal -->
<div class="modal fade" id="myLogin" role="dialog">
	<div class="modal-dialog"> 
		
		<!-- Modal content-->
		<div class="modal-content text-center">
			<div class="modal-body">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Sign in to <br>
					<strong><span class="glyphicon glyphicon-link"></span> Menter-Mentee</strong></h4>
				<br>
				<div class="container-fluid text-left">
					<div class="row">
						<div class="col-sm-6 col-sm-offset-3">
							<div class="panel panel-default">
								<div class="panel-body">
									<form>
										<div class="form-group">
											<label for="email">Username or email address</label>
											<input type="email" class="form-control" id="email">
										</div>
										<div class="form-group clearfix">
											<label for="pwd" class="pull-left">Password</label>
											<p class="pull-right"><a href="" class="small">Forgot password?</a></p>
											<input type="password" class="form-control" id="pwd">
										</div>
										<div class="checkbox">
											<label>
												<input type="checkbox">
												Remember me</label>
										</div>
										<div class="form-group">
											<button type="submit" class="btn btn-block btn-primary">Sign in</button>
										</div>
										<span class="small">Sign in with:</span>
										<div class="form-group">
											<div class="btn-group btn-group-justified">
												<a href="inc/fblogin.php" class="btn btn-primary"><i class="devicon-facebook-plain"></i></a>
												<a href="#" class="btn btn-info"><i class="devicon-twitter-plain"></i></a>
												<a href="#" class="btn btn-danger"><i class="devicon-google-plain"></i></a>
												<a href="#" class="btn btn-default"><i class="devicon-github-plain"></i></a>
											</div>
											<!--<span class="small">By clicking "Sign up", you agree to our <a href="">terms of use</a> and <a href="">privacy policy</a>.</span>-->
										</div>
									</form>
								</div>
								<div class="panel-footer text-center"> <span class="small">New to Menter-Mentee? <a href="">Create an account.</a></span> </div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div><!-- / .modal-content -->
		
	</div>
</div><!-- / .modal -->
