<?php require('header.php'); ?>

<!-- contents -->
<section id="service">
	<div class="container">
		<div class="row row-1">
			<div class="col-md-4 ser-col-4">
				<div class="ser-col ser-1">
					<div class="icon-col">
						<i class="devicon-wordpress-plain-wordmark colored"></i>
					</div>
					<h3>this is a loerem spe</h3>
					<p>Lorem ipsum dolor sit amet, consectetur
						adipiscing elit. Curabitur
						sit amet enim mauris. Fusce
						hendrerit velit vitae enim
						hendrerit ultrices. </p>
					<a href="page.php" class="btn btn-link"><span class="glyphicon glyphicon-play-circle"></span> Read More</a>
					<i class="fa fa-circle-thin" aria-hidden="true"></i>
					</span>
				</div>
			</div>

			<div class="col-md-4 ser-col-4">
				<div class="ser-col ser-2">
					<div class="icon-col">
						<i class="devicon-bootstrap-plain-wordmark colored"></i>
					</div>
					<h3>this is a loerem spe</h3>
					<p>Lorem ipsum dolor sit amet, consectetur
						adipiscing elit. Curabitur
						sit amet enim mauris. Fusce
						hendrerit velit vitae enim
						hendrerit ultrices. </p>
					<a href="page.php" class="btn btn-link"><span class="glyphicon glyphicon-play-circle"></span> Read More</a>
					<i class="fa fa-circle-thin" aria-hidden="true"></i>
					</span>
				</div>
			</div>

			<div class="col-md-4 ser-col-4-l">
				<div class="ser-col ser-3">
					<div class="icon-col">
						<i class="devicon-bitbucket-plain-wordmark colored"></i>
					</div>
					<h3>this is a loerem spe</h3>
					<p>Lorem ipsum dolor sit amet, consectetur
						adipiscing elit. Curabitur
						sit amet enim mauris. Fusce
						hendrerit velit vitae enim
						hendrerit ultrices. </p>
					<a href="page.php" class="btn btn-link"><span class="glyphicon glyphicon-play-circle"></span> Read More</a>
				</div>
			</div>
		</div>
		<!--=====row 1============-->

		<!--====row 2============-->
		<div class="row row-2">
			<div class="col-md-4 ser-col-4">
				<div class="ser-col ser-4">
					<div class="icon-col">
						<i class="devicon-css3-plain-wordmark colored"></i>
					</div>
					<h3>this is a loerem spe</h3>
					<p>Lorem ipsum dolor sit amet, consectetur
						adipiscing elit. Curabitur
						sit amet enim mauris. Fusce
						hendrerit velit vitae enim
						hendrerit ultrices. </p>
					<a href="page.php" class="btn btn-link"><span class="glyphicon glyphicon-play-circle"></span> Read More</a>
					<i class="fa fa-circle-thin" aria-hidden="true"></i>
					</span>
				</div>
			</div>

			<div class="col-md-4 ser-col-4">
				<div class="ser-col ser-5">
					<div class="icon-col">
						<i class="devicon-git-plain-wordmark colored"></i>
					</div>
					<h3>this is a loerem spe</h3>
					<p>Lorem ipsum dolor sit amet, consectetur
						adipiscing elit. Curabitur
						sit amet enim mauris. Fusce
						hendrerit velit vitae enim
						hendrerit ultrices. </p>
					<a href="page.php" class="btn btn-link"><span class="glyphicon glyphicon-play-circle"></span> Read More</a>
					<i class="fa fa-circle-thin" aria-hidden="true"></i>
					</span>
				</div>
			</div>

			<div class="col-md-4 ser-col-4-l">
				<div class="ser-col ser-6">
					<div class="icon-col">
						<i class="devicon-html5-plain colored"></i>
					</div>
					<h3>this is a loerem spe</h3>
					<p>Lorem ipsum dolor sit amet, consectetur
						adipiscing elit. Curabitur
						sit amet enim mauris. Fusce
						hendrerit velit vitae enim
						hendrerit ultrices. </p>
					<a href="page.php" class="btn btn-link"><span class="glyphicon glyphicon-play-circle"></span> Read More</a>
				</div>
			</div>
		</div>
		<!--=======row 2============-->

		<!--    row 3============-->
		<div class="row row-3">
			<div class="col-md-4 ser-col-4">
				<div class="ser-col ser-7">
					<div class="icon-col">
						<i class="devicon-javascript-plain colored"></i>
					</div>
					<h3>this is a loerem spe</h3>
					<p>Lorem ipsum dolor sit amet, consectetur
						adipiscing elit. Curabitur
						sit amet enim mauris. Fusce
						hendrerit velit vitae enim
						hendrerit ultrices. </p>
					<a href="page.php" class="btn btn-link"><span class="glyphicon glyphicon-play-circle"></span> Read More</a>
				</div>
			</div>

			<div class="col-md-4 ser-col-4">
				<div class="ser-col ser-8">
					<div class="icon-col">
						<i class="devicon-jquery-plain-wordmark colored"></i>
					</div>
					<h3>this is a loerem spe</h3>
					<p>Lorem ipsum dolor sit amet, consectetur
						adipiscing elit. Curabitur
						sit amet enim mauris. Fusce
						hendrerit velit vitae enim
						hendrerit ultrices. </p>
					<a href="page.php" class="btn btn-link"><span class="glyphicon glyphicon-play-circle"></span> Read More</a>
				</div>
			</div>

			<div class="col-md-4 ser-col-4-l">
				<div class="ser-col ser-9">
					<div class="icon-col">
						<i class="devicon-linux-plain colored"></i>
					</div>
					<h3>this is a loerem spe</h3>
					<p>Lorem ipsum dolor sit amet, consectetur
						adipiscing elit. Curabitur
						sit amet enim mauris. Fusce
						hendrerit velit vitae enim
						hendrerit ultrices. </p>
					<a href="page.php" class="btn btn-link"><span class="glyphicon glyphicon-play-circle"></span> Read More</a>
				</div>
			</div>
		</div>
		<!--=======row 3============-->

		<!--    row 4============-->
		<div class="row row-4">
			<div class="col-md-4 ser-col-4">
				<div class="ser-col ser-10">
					<div class="icon-col">
						<i class="devicon-mysql-plain-wordmark colored"></i>
					</div>
					<h3>this is a loerem spe</h3>
					<p>Lorem ipsum dolor sit amet, consectetur
						adipiscing elit. Curabitur
						sit amet enim mauris. Fusce
						hendrerit velit vitae enim
						hendrerit ultrices. </p>
					<a href="page.php" class="btn btn-link"><span class="glyphicon glyphicon-play-circle"></span> Read More</a>
				</div>
			</div>

			<div class="col-md-4 ser-col-4">
				<div class="ser-col ser-11">
					<div class="icon-col">
						<i class="devicon-php-plain colored"></i>
					</div>
					<h3>this is a loerem spe</h3>
					<p>Lorem ipsum dolor sit amet, consectetur
						adipiscing elit. Curabitur
						sit amet enim mauris. Fusce
						hendrerit velit vitae enim
						hendrerit ultrices. </p>
					<a href="page.php" class="btn btn-link"><span class="glyphicon glyphicon-play-circle"></span> Read More</a>
				</div>
			</div>

			<div class="col-md-4 ser-col-4-l">
				<div class="ser-col ser-12">
					<div class="icon-col">
						<i class="devicon-ubuntu-plain-wordmark colored"></i>
					</div>
					<h3>this is a loerem spe</h3>
					<p>Lorem ipsum dolor sit amet, consectetur
						adipiscing elit. Curabitur
						sit amet enim mauris. Fusce
						hendrerit velit vitae enim
						hendrerit ultrices. </p>
					<a href="page.php" class="btn btn-link"><span class="glyphicon glyphicon-play-circle"></span> Read More</a>
				</div>
			</div>
		</div>
		<!--=======row 4============-->

	</div>
</section>

<?php require('footer.php'); ?>