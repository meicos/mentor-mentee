<!-- footer -->
<footer class="container-fluid text-center">
	<div class="footer" id="footer">
		<div class="container">
			<div class="row">
				<div class="col-sm-2 text-left">
					<h4>Menter-Mentee </h4>
					<ul>
						<li> <a href="#"> Courses </a> </li>
						<li> <a href="#"> Screencasts </a> </li>
						<li> <a href="#"> Blog </a> </li>
						<li> <a href="#"> Pricing </a> </li>
						<li> <a href="#"> For Business </a> </li>
					</ul>
				</div>
				<div class="col-sm-2 text-left">
					<h4> About Us </h4>
					<ul>
						<li> <a href="#"> Company </a> </li>
						<li> <a href="#"> Jobs </a> </li>
						<li> <a href="#"> Press </a> </li>
						<li> <a href="#"> Terms of Use </a> </li>
						<li> <a href="#"> Privacy Policy </a> </li>
					</ul>
				</div>
				<div class="col-sm-2 text-left">
					<h4> Need Help? </h4>
					<ul>
						<li> <a href="#"> Contact Us </a> </li>
						<li> <a href="#"> Knowledge Base </a> </li>
						<li> <a href="#"> Forum </a> </li>
					</ul>
				</div>
				<div class="col-sm-5 col-sm-offset-1 text-left">
					<h4><span class="glyphicon glyphicon-link"></span> Menter-Mentee <small>© 2017</small></h4>
					<p>Menter-Mentee is a place for the people who inspired by the way you work. From <strong>beginner</strong> to <strong>advance</strong>, you can share tha way how to learn programming alongside millions of other developers.</p>
					<p><a href=""><strong><span class="	glyphicon glyphicon-play-circle"></span> Learn More</strong></a></p>
				</div>
			</div>
		</div>
	</div>
</footer>

<?php require('modal_signin.php'); ?>

</body>
</html>